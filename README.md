# Trader Hut

This Angular application make uses of the [Mongo Express Demo server](https://bitbucket.org/rajinder_yadav/mongo-express). Make sure to fetch and build this project first.

**Trader Hut** application was created using `angular-cli`, so the same commands apply here.

To run the demo app use, `ng serve`

Click on the **login** button to view the customer orders. The application makes use of OAuth0 to perform the login authorization. It also adds a router guard to be able to view customer orders.

## Main view
![image](img/main.png)

## OAuth login
![image](img/login.png)

## Post login
![image](img/logged-in.png)

## Order view
![image](img/order-view.png)
