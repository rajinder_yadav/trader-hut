import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'th-product-item',
  template: `
  <div class="bb">
    <span class="f4 w-70 fl">{{name}}</span>
    <span class="f4 w-30">{{cost}}</span>
  </div>
  `,
  styles: []
})
export class ProductItemComponent implements OnInit {

  @Input() name: string;
  @Input() cost: number;

  constructor() { }

  ngOnInit() {
  }

}
