import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Http } from '@angular/http';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

declare var google: any;

@Component({
  selector: 'th-customer-order',
  template: `
<div class="cf">
  <span *ngIf="customer" class="fl f2">
    <img src="http://localhost:3000/img/{{customer?.firstName|lowercase}}-{{customer?.lastName|lowercase}}.jpg"
  class="w-10 fl pr3 img-sm b"/>Trader's Information
  </span>
</div>
<p class="f4 mt3 ml3">
  Name: {{customer?.firstName}} {{customer?.lastName}}
</p>
<p class="f4 ml3 mb0">Location</p>
<div class="mt1 ml3 mb4" id="map"></div>
<div class="ml3 mr3 w-40">
<div *ngIf="order">
  <div class="bb">
    <span class="f4 w-70 fl b">Product</span>
    <span class="f4 w-30 b">Cost</span>
  </div>
<p *ngFor="let o of order?.orders">
  <th-product-item [name]="o.productName" [cost]="o.itemCost"></th-product-item>
</p>
<span class="f4 w-70 fl b">Total</span><span class="f4 w-30 b">{{totalCost}}</span>
</div>
<p><button (click)="onBack()" class="mt4 fr ba br3 white bg-dark-blue">Back</button></p>
</div>
  `,
  styles: [`
    .img-sm {
      width: 50px;
    }
    #map {
      width: 30%;
      height: 250px;
      background-color: grey;
    }
  `]
})
export class CustomerOrderComponent implements OnInit {

  customer: any;
  order: any;
  params: Params;
  totalCost: number;

  constructor(
    private http: Http,
    private route: ActivatedRoute,
    private location: Location) {
      this.route.params.subscribe((p: Params) => {
        this.params = p;
        this.http.get(`http://localhost:3000/customer/${p.id}`)
        .switchMap(res => res.json())
        .subscribe(v => {
          this.customer = v;
          const uluru = {lat: this.customer.latitude, lng: this.customer.longitude};
          const map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: uluru
          });
          const marker = new google.maps.Marker({
            position: uluru,
            map: map
          });
        });
      });
      this.http.get(`http://localhost:3000/order/${this.params.id}`)
      .switchMap(res => res.json())
      .subscribe(v => {
        this.order = v;
        if(this.order.orders.length > 0) {
          this.totalCost = this.order.orders.reduce((acc, val) => {
            return acc + val.itemCost;
          }, 0).toFixed(2);
          console.log(this.totalCost);
        }
      });
    }

  ngOnInit() {
  }

  onBack() {
    this.location.back();
  }
}
