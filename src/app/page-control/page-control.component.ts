import { Component, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'th-page-control',
  template: `
<span *ngFor="let link of links; index as i">\
<a [routerLink]="link"
  routerLinkActive="active"
  class="ba pl2 pr2 dark-blue bg-white ma0 link hover-bg-light-blue">{{i+1}}</a></span>
  `,
  styles: [`
    .active {
      background-color: #00449e;
      color: #fff;
      border-color: #00449e;
    },
    a {
      outline: 0;
    }
    a:active,
    a:focus {
      outline: none;
    }
  `]
})
export class PageControlComponent implements OnChanges {

  @Input() uri: string;
  @Input() pages: number;

  links: string[] = [];

  constructor() { }

  ngOnChanges() {
    for (let i = 0; i <= this.pages; ++i) {
      this.links.push(`/${this.uri}/${i}`);
    }
  }

}
