import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Params } from '@angular/router';
import { CustomerListItemComponent } from '../customer-list-item/customer-list-item.component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
// import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'th-customer-list',
  template: `
  <div class="ml3">
    <th-page-control uri="/customers" [pages]="pages"></th-page-control>
  </div>
  <div class="cf pt1 pb1 f4 v-mid bb">
    <span class="w-10 fl pr3 img-sm">&nbsp;</span>
    <div class="mt3">
      <span class="w-10 fl b">Name</span>
      <span class="w-10 fl b">Surname</span>
      <span class="w-20 fl b">Address</span>
      <span class="w-10 fl b">City</span>
      <span class="w-10 fl b">State</span>
    </div>
  </div>
    <div *ngFor="let c of customer$|async; even as isEven">
      <th-customer-list-item [customer]="c" [even]="isEven"></th-customer-list-item>
    </div>
  `,
  styles: [`
    .img-sm {
      max-width: 50px;
    }
  `]
})
export class CustomerListComponent implements OnInit {

  customer$;
  page: number;
  pages: number;

  constructor(
    private http: Http,
    private route: ActivatedRoute) {
      this.route.params.subscribe((params: Params) => {
      this.page = params.id;
      this.http.get('http://localhost:3000/customer/page/count').toPromise().then(v => {
        this.pages = parseInt(v.json().count);
      });
      this.customer$ = this.http.get(`http://localhost:3000/customer/page/${this.page}`).map(res => res.json());
    });
  }

  ngOnInit() {
  }

}
