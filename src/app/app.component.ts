import { Component, OnInit } from '@angular/core';
import { PageControlComponent } from './page-control/page-control.component';
import { Auth } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Trader Hut';

  constructor(private auth: Auth) {
  }

  ngOnInit() {
  }
}
