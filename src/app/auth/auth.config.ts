interface AuthConfiguration {
  clientID: string;
  domain: string;
}

export const authConfig: AuthConfiguration = {
  clientID: 'g42TJ363eKxml43R4PfTeFNHOBZHuOGT',
  domain: 'rajinder.auth0.com'
};
