import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { Auth } from './auth/auth.service';

import { AppComponent } from './app.component';
import { CustomerListItemComponent } from './customer-list-item/customer-list-item.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { PageControlComponent } from './page-control/page-control.component';
import { CustomerOrderComponent } from './customer-order/customer-order.component';
import { ProductItemComponent } from './product-item/product-item.component';

import { AuthGuard } from './auth/auth-guard.service';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'customers/0',
    pathMatch: 'full'
  },
  {
    path: 'customers/:id',
    component: CustomerListComponent,
  },
  {
    path: 'order/:id',
    component: CustomerOrderComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    CustomerListItemComponent,
    CustomerListComponent,
    PageControlComponent,
    CustomerOrderComponent,
    ProductItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot( appRoutes )
  ],
  providers: [
    Auth,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
