import { Component, OnInit, Input} from '@angular/core';
import { Auth } from "../auth/auth.service";

@Component({
  selector: 'th-customer-list-item',
  template: `
  <div class="cf pt1 pb1 f4 v-mid bb" [ngStyle]="{'background-color': even?'#f2f2f2':'#f4fbff'}">
    <img src="http://localhost:3000/img/{{customer?.firstName|lowercase}}-{{customer?.lastName|lowercase}}.jpg"
      class="w-10 fl pr3 img-sm"/>
    <div class="mt3">
      <span class="w-10 fl">{{customer.firstName}}</span>
      <span class="w-10 fl">{{customer.lastName}}</span>
      <span class="w-20 fl">{{customer.address}}</span>
      <span class="w-10 fl">{{customer.city}}</span>
      <span class="w-10 fl">{{customer.state.name}}</span>
      <span *ngIf="auth.authenticated()" class="w-10 fl f6"><a [routerLink]="['/order/', customer.id]"
        class="ba br4 pa2 link b--blue bg-blue white">View Order</a></span>
    </div>
  </div>
  `,
  styles: [`
    .img-sm {
      width: 50px;
    }
  `]
})
export class CustomerListItemComponent implements OnInit {

  @Input() customer;
  @Input() even: boolean;

  constructor(private auth: Auth) { }

  ngOnInit() {
  }

}
