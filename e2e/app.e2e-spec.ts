import { TraderHutPage } from './app.po';

describe('trader-hut App', () => {
  let page: TraderHutPage;

  beforeEach(() => {
    page = new TraderHutPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
